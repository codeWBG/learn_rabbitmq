package org.bg.rabbitmq;


import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class Publisher {

    @Autowired
    private RabbitTemplate template;

    @Test
    public void publish_normal() {
        template.convertAndSend(RabbitMQConfig.EXCHANGE, RabbitMQConfig.ROUTING_KEY, "hello msg");
    }

    /**
     * 5秒未被消费会路由到死信队列
     */
    @Test
    public void publish_expir() {
        template.convertAndSend(RabbitMQConfig.EXCHANGE, RabbitMQConfig.ROUTING_KEY, "hello expir dead", message -> {
            message.getMessageProperties().setExpiration("5000");
            return message;
        });
    }

    /**
     * 开启confirm、return机制
     */
    @Test
    public void publish_arg() {
        template.setMandatory(true);
        template.setConfirmCallback((correlationData, ack, exe) -> {
            if (ack) System.out.println("交换机接收到了消息");
            else System.err.println("交换机未接收到消息" + exe);
        });
        template.setReturnsCallback(returnedMessage -> {
            System.err.println("消息未路由到队列");
        });
        template.convertAndSend(RabbitMQConfig.EXCHANGE, RabbitMQConfig.ROUTING_KEY, "hello dead", message -> {
            message.getMessageProperties().setCorrelationId("我是唯一标识");
            message.getMessageProperties().setMessageId("消息ID");
            return message;
        });
    }
}
